package dhcpve

import (
	"github.com/coredhcp/coredhcp/handler"
	"github.com/coredhcp/coredhcp/logger"
	"github.com/coredhcp/coredhcp/plugins"
	"github.com/insomniacslk/dhcp/dhcpv4"
	"github.com/insomniacslk/dhcp/dhcpv6"
	"gitlab.com/disappointment-industries/dhcpve/pve"
	"net"
	"time"
)

var log = logger.GetLogger("dhcpve")

var Plugin = plugins.Plugin{
	Name:   "dhcpve",
	Setup6: setup6,
	Setup4: setup4,
}

func setup6(args ...string) (handler.Handler6, error) {
	log.Printf("loaded plugin for DHCPv6.")
	return Handler6, nil
}

func setup4(args ...string) (handler.Handler4, error) {
	log.Printf("loaded plugin for DHCPv4.")
	return Handler4, nil
}

func Handler6(req, resp dhcpv6.DHCPv6) (dhcpv6.DHCPv6, bool) {
	m, err := req.GetInnerMessage()
	if err != nil {
		log.Errorf("BUG: could not decapsulate: %v", err)
		return nil, true
	}

	if m.Options.OneIANA() == nil {
		log.Debug("No address requested")
		return resp, false
	}

	mac, err := dhcpv6.ExtractMAC(req)
	if err != nil {
		log.Warningf("Could not find client MAC, passing")
		return resp, false
	}
	log.Debugf("looking up an IP address for MAC %s", mac.String())

	tries := 0
	ipaddr, ok := "", false
	for tries < 2 && !ok {
		ipaddr, ok = pve.IDs[mac.String()]
		e := Refresh()
		if e != nil {
			log.Warning(e.Error())
		}
		tries++
	}
	if !ok {
		log.Warningf("MAC address %s is unknown", mac.String())
		return resp, false
	}
	ipaddr = "fd00::" + ipaddr
	ip := net.ParseIP(ipaddr)
	if ip == nil {
		log.Warningf("bad ip (%s) for mac %s", ipaddr, mac.String())
		return resp, false
	}
	log.Debugf("found IP address %s for MAC %s", ip, mac.String())

	resp.AddOption(&dhcpv6.OptIANA{
		IaId: m.Options.OneIANA().IaId,
		Options: dhcpv6.IdentityOptions{Options: []dhcpv6.Option{
			&dhcpv6.OptIAAddress{
				IPv6Addr:          ip,
				PreferredLifetime: 3600 * time.Second,
				ValidLifetime:     3600 * time.Second,
			},
		}},
	})
	return resp, false
}

func Handler4(req, resp *dhcpv4.DHCPv4) (*dhcpv4.DHCPv4, bool) {
	tries := 0
	ipaddr, ok := "", false
	for tries < 2 && !ok {
		ipaddr, ok = pve.IDs[req.ClientHWAddr.String()]
		e := Refresh()
		if e != nil {
			log.Warning(e.Error())
		}
		tries++
	}
	if !ok {
		log.Warningf("MAC address %s is unknown", req.ClientHWAddr.String())
		return resp, false
	}
	ipaddr = "192.168.0." + ipaddr
	ip := net.ParseIP(ipaddr)
	if ip == nil {
		log.Warningf("bad ip (%s) for mac %s", ipaddr, req.ClientHWAddr.String())
		return resp, false
	}
	resp.YourIPAddr = ip
	log.Debugf("found IP address %s for MAC %s", ip, req.ClientHWAddr.String())
	return resp, true
}
