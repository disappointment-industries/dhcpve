package dhcpve

import (
	"gitlab.com/disappointment-industries/dhcpve/pve"
	"time"
)

var lastfresh = time.Now().Add(-1 * time.Minute)

func Refresh() error {
	if time.Now().Sub(lastfresh).Seconds() > 5 {
		e := pve.Refresh()
		if e != nil {
			return e
		}
		lastfresh = time.Now()
	}
	return nil
}
