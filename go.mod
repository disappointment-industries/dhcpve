module gitlab.com/disappointment-industries/dhcpve

replace gitlab.com/disappointment-industries/dhcpve => ./

go 1.15

require (
	github.com/coredhcp/coredhcp v0.0.0-20210118192424-9e42cc2ca008
	github.com/insomniacslk/dhcp v0.0.0-20201112113307-4de412bc85d8
	gitlab.com/MikeTTh/env v0.0.0-20210102155928-2e9be3823cc7
	golang.org/x/net v0.0.0-20201110031124-69a78807bb2b
	golang.org/x/sys v0.0.0-20210113000019-eaf3bda374d2 // indirect
)
