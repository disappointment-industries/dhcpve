package main

import (
	"fmt"
	"gitlab.com/disappointment-industries/dhcpve/pve"
)

func main() {
	e := pve.Refresh()
	if e != nil {
		fmt.Println(e)
	}

	for k, v := range pve.IDs {
		fmt.Printf("%s -> %s\n", k, v)
	}
}
