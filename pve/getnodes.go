package pve

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

const path = "/api2/json/nodes/"

type nodes struct {
	Data []struct {
		Node string
	}
}

func GetNodes() ([]string, error) {
	r, e := http.Get(URL + path)
	if e != nil {
		return nil, e
	}

	b, e := ioutil.ReadAll(r.Body)
	if e != nil {
		return nil, e
	}

	var n nodes
	e = json.Unmarshal(b, &n)
	if e != nil {
		return nil, e
	}

	ret := make([]string, len(n.Data))

	for i, node := range n.Data {
		ret[i] = URL + path + node.Node
	}

	return ret, nil
}
