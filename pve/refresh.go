package pve

func Refresh() error {
	nodes, e := GetNodes()
	if e != nil {
		return e
	}

	for _, n := range nodes {
		cts, e := GetCTs(n)
		if e != nil {
			return e
		}

		for _, c := range cts {
			e := GetMacs(n, ctPath, c)
			if e != nil {
				return e
			}
		}

		vms, e := GetVMs(n)
		if e != nil {
			return e
		}

		for _, c := range vms {
			e := GetMacs(n, vmPath, c)
			if e != nil {
				return e
			}
		}
	}

	return nil
}
