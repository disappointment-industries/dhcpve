package pve

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

const confPath = "/config"

func GetMacs(nodeURL, prefix, ctID string) error {
	r, e := http.Get(nodeURL + prefix + ctID + confPath)
	if e != nil {
		return e
	}

	b, e := ioutil.ReadAll(r.Body)
	if e != nil {
		return e
	}

	type res struct {
		Data map[string]interface{}
	}
	var re res
	e = json.Unmarshal(b, &re)
	if e != nil {
		return e
	}
	conf := re.Data

	for k, v := range conf {
		if len(k) >= 3 && k[:3] == "net" {
			pairs := strings.Split(fmt.Sprint(v), ",")
			for _, p := range pairs {
				pair := strings.Split(p, "=")
				if len(pair) >= 2 {
					key := pair[0]
					val := pair[1]

					if key == "hwaddr" || key == "virtio" {
						lower := strings.ToLower(val)
						upper := strings.ToUpper(val)
						IDs[lower] = ctID
						IDs[upper] = ctID
					}
				}
			}
		}
	}

	return nil
}
