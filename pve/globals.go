package pve

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"gitlab.com/MikeTTh/env"
	"io/ioutil"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"time"
)

var IDs = make(map[string]string)

var URL = env.String("PVE_URL", "https://192.168.0.1:8006")

func doPanic(env string) func() string {
	return func() string {
		panic(fmt.Sprintf("env var %s missing", env))
	}
}

func init() {
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	r, e := http.PostForm(URL+"/api2/json/access/ticket", url.Values{
		"username": []string{env.GetOrDosomething("PVE_USER", doPanic("PVE_USER"))},
		"password": []string{env.GetOrDosomething("PVE_PASS", doPanic("PVE_PASS"))},
	})

	type resp struct {
		Data struct {
			CSRFPreventionToken string
			Ticket              string
		}
	}
	if e != nil {
		panic(e)
	}

	b, e := ioutil.ReadAll(r.Body)
	if e != nil {
		panic(e)
	}

	var re resp
	e = json.Unmarshal(b, &re)
	if e != nil {
		panic(e)
	}

	jar, e := cookiejar.New(nil)
	if e != nil {
		panic(e)
	}
	u, e := url.Parse(URL)
	if e != nil {
		panic(e)
	}
	jar.SetCookies(u, []*http.Cookie{
		{
			Name:    "PVEAuthCookie",
			Value:   re.Data.Ticket,
			Expires: time.Now().AddDate(1, 0, 0),
		},
	})
	http.DefaultClient.Jar = jar
}
