package pve

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

const ctPath = "/lxc/"
const vmPath = "/qemu/"

type resp struct {
	Data []struct {
		VMID string
	}
}

func getCTOrVM(ct bool) func(string) ([]string, error) {
	return func(nodeURL string) ([]string, error) {
		path := vmPath
		if ct {
			path = ctPath
		}
		r, e := http.Get(nodeURL + path)
		if e != nil {
			return nil, e
		}

		b, e := ioutil.ReadAll(r.Body)
		if e != nil {
			return nil, e
		}

		var c resp
		e = json.Unmarshal(b, &c)
		if e != nil {
			return nil, e
		}

		ret := make([]string, len(c.Data))

		for i, cont := range c.Data {
			ret[i] = cont.VMID
		}

		return ret, nil
	}
}

var GetCTs = getCTOrVM(true)
var GetVMs = getCTOrVM(false)
